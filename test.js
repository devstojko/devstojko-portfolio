const container = document.getElementById("face-canvas");

var app = new PIXI.Application({
  transparent: true,
  resizeTo: container
});

container.appendChild(app.view);

app.stage.alpha = 1;
app.stage.interactive = true;

const real = PIXI.Texture.from("./assets/real.png");
const mask = PIXI.Texture.from("./assets/test_1.jpg");

var layer1 = new PIXI.Sprite(real);

layer1.width = 500;
layer1.height = 500;
layer1.x = document.body.offsetWidth / 2 - layer1.width;
layer1.y = 0;

// var layer2 = new PIXI.Sprite(real);

// layer2.width = 400;
// layer2.height = 400;
// layer2.x = document.body.offsetWidth / 2 - layer2.width;
// layer2.y = 0;

var displacementFilterTexture = new PIXI.Sprite(mask);

displacementFilterTexture.width = 500;
displacementFilterTexture.height = 500;
displacementFilterTexture.x =
  document.body.offsetWidth / 2 - displacementFilterTexture.width;
displacementFilterTexture.y = 0;

var mainContainer = new PIXI.Container();

mainContainer.addChild(layer1);
mainContainer.addChild(displacementFilterTexture);

var additionalContainer = new PIXI.Container();
// additionalContainer.addChild(layer2);

var circle = new PIXI.Graphics();
circle.lineStyle(0);
circle.beginFill(0xff3300, 0);
circle.drawCircle(0, 0, 150);
circle.endFill();
mainContainer.addChild(circle);

additionalContainer.mask = circle;
mainContainer.addChild(additionalContainer);

var blurFilter = new PIXI.filters.BlurFilter(10, 8, 0, 0);

var displacementFilter = new PIXI.filters.DisplacementFilter(
  displacementFilterTexture,
  6
);

displacementFilter.blendMode = PIXI.BLEND_MODES.LIGHTEN;

layer1.filters = [displacementFilter];
// layer2.filters = [displacementFilter];

app.stage.addChild(mainContainer);

setMoveHandlers(app, circle, displacementFilter);

function setMoveHandlers(app, circle, displacementFilter) {
  app.stage
    .on("mousemove", onPointerMove.bind(null, circle, displacementFilter))
    .on("touchmove", onPointerMove.bind(null, circle, displacementFilter));
}

function onPointerMove(circle, displacementFilter, eventData) {
  circle.position.set(eventData.data.global.x, eventData.data.global.y);
  setTilt(
    6,
    eventData.data.global.x,
    eventData.data.global.y,
    displacementFilter
  );
}

function setTilt(maxTilt, mouseX, mouseY, displacementFilter) {
  var midpointX = document.body.offsetWidth / 2,
    midpointY = document.body.offsetHeight / 2,
    posX = midpointX - mouseX,
    posY = midpointY - mouseY,
    valX = (posX / midpointX) * maxTilt,
    valY = (posY / midpointY) * maxTilt;
  displacementFilter.scale.x = valX;
  displacementFilter.scale.y = valY;
}
